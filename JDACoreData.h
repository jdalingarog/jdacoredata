//
//  JDACoreData.h
//  WomensCircle
//
//  Created by John Dwaine Alingarog on 11/2/13.
//  Copyright (c) 2013 John Dwaine Alingarog. All rights reserved.
//
// Normal init will return nil. Use either managerWithContext: or managerWithDocumentName: class

#import <Foundation/Foundation.h>

@interface JDACoreData : NSObject

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) UIManagedDocument *document;

//Shared
+ (JDACoreData *)sharedManager;

//Use this if you're not going to use UIManagedDocument
+ (JDACoreData *)managerWithContext:(NSManagedObjectContext *)context;

//Use this if you're going to use UIManagedDocument
+ (JDACoreData *)managerWithDocumentName:(NSString *)documentName;
- (void)createManagedDocumentWithCompletionHandler:(void (^)(BOOL success))completionHandler;

//Delete methods
- (void)deleteStoreURLWithString:(NSString *)storeURLString;

@end
