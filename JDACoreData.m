//
//  JDACoreData.m
//  WomensCircle
//
//  Created by John Dwaine Alingarog on 11/2/13.
//  Copyright (c) 2013 John Dwaine Alingarog. All rights reserved.
//

#import "JDACoreData.h"
#import <CoreData/CoreData.h>

@interface JDACoreData ()
@property (strong, nonatomic) NSString *documentName;
@end

@implementation JDACoreData

static JDACoreData *sharedJDACoreData;

#pragma mark - Shared

+ (JDACoreData *)sharedManager
{
    @synchronized(self) {
        if (!sharedJDACoreData) sharedJDACoreData = [[JDACoreData alloc] init];
        return sharedJDACoreData;
    }
}

+ (JDACoreData *)managerWithContext:(NSManagedObjectContext *)context
{
    @synchronized(self) {
        if (!sharedJDACoreData) sharedJDACoreData = [[JDACoreData alloc] initWithContext:context];
        return sharedJDACoreData;
    }
}

+ (JDACoreData *)managerWithDocumentName:(NSString *)documentName
{
    @synchronized(self) {
        if (!sharedJDACoreData) sharedJDACoreData = [[JDACoreData alloc] initWithDocumentName:documentName];
        return sharedJDACoreData;
    }
}

#pragma mark - Constructors

- (instancetype)init
{
    return nil;
}

- (instancetype)initWithDocumentName:(NSString *)documentName
{
    self = [super init];
    if (self) {
        self.documentName = documentName;
    }
    return self;
}

- (instancetype)initWithContext:(NSManagedObjectContext *)context
{
    self = [super init];
    if (self) {
        self.context = context;
    }
    return self;
}

#pragma mark - Create document method

- (void)createManagedDocumentWithCompletionHandler:(void (^)(BOOL success))completionHandler {
    if (!self.documentName) {
        NSLog(@"Use managerWithDocumentName: method before calling createManagedDocument: block");
        completionHandler(NO);
    }
    
    //Get document url
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsDirectory = [[fileManager URLsForDirectory:NSDocumentDirectory
                                                     inDomains:NSUserDomainMask] firstObject];
    NSURL *url = [documentsDirectory URLByAppendingPathComponent:self.documentName];
    self.document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    //Check if file exists
    BOOL fileExists = [fileManager fileExistsAtPath:url.path];
    
    if (!fileExists) {
        //Create file if it doesn't exists
        [self.document saveToURL:url
                forSaveOperation:UIDocumentSaveForCreating
               completionHandler:^(BOOL success) {
                   if (success) self.context = self.document.managedObjectContext;
                   if (!success) NSLog(@"Couldn't open document at %@", url);
                   completionHandler(success); //complete block
               }];
    } else {
        //Open document if file exits
        [self.document openWithCompletionHandler:^(BOOL success) {
            if (success) self.context = self.document.managedObjectContext;
            if (!success) NSLog(@"Couldn't create document at %@", url);
            completionHandler(success); //complete block
        }];
    }
}

#pragma mark - Delete methods

- (void)deleteStoreURLWithString:(NSString *)storeURLString
{
    NSError *error;
    NSFileManager *defaultManager          = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory   = [[defaultManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL                        = [applicationDocumentsDirectory URLByAppendingPathComponent:storeURLString];
    [[NSFileManager defaultManager] removeItemAtPath:storeURL.path error:&error];
}

@end
